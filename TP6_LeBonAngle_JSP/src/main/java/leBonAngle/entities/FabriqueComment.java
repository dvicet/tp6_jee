package leBonAngle.entities;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class FabriqueComment {
	
	private static final String CHAMP_PRIX = "prix";
	private static final String CHAMP_COMMENTAIRE = "comment";
	private static final String CHAMP_Id_AD = "id_ad";
	private Map<String, String> erreurs = new HashMap<String, String>();
	public boolean succesCreation;
	
	
	public Comment createComment(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		int p = Integer.parseInt(request.getParameter(CHAMP_PRIX));
		String c = request.getParameter(CHAMP_COMMENTAIRE);
		int id_ad = Integer.parseInt(request.getParameter("id"));
		
		if(session.getAttribute("user_id") == null)
		{
			this.erreurs.put("SESSION", "Vous n'êtes pas connecté");
		}
		
		try
		{
			this.validPrix(p);
			
		}
		catch(Exception e)
		{
			this.erreurs.put(CHAMP_PRIX, e.getMessage());
		}
		try
		{
			this.validIdAd(id_ad);
			
		}
		catch(Exception e)
		{
			this.erreurs.put(CHAMP_Id_AD, e.getMessage());
		}
		
		int prix = p;
		String comment = c;
		this.succesCreation = this.erreurs.isEmpty();
		
		if(this.succesCreation)
		{
			Comment commentaire = new Comment();
			commentaire.setPrice_proposed(prix);
			commentaire.setComment(comment);
			commentaire.setId_advertisement(id_ad);
			commentaire.setId_user(Integer.parseInt(session.getAttribute("user_id").toString()));
			return commentaire;
		}
		else
		{
			return null;
		}
	}
	
	public void validPrix(int prix) throws Exception
	{
		if(prix < 0)
		{
			throw new Exception("Ce n'est pas gratuit !");
		}
	}
	
	public void validIdAd(int id) throws Exception
	{
		if(id < 0)
		{
			throw new Exception("Annonce non trouvée");
		}
	}
	
	public String getMap(String key)
	{
		return this.erreurs.get(key);
	}
	
	
	
}
