package leBonAngle.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;

import leBonAngle.entities.Advertisement;
import leBonAngle.entities.User;

@WebServlet("/depose")
public class Depose extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
	public Depose() {
        super();
    }
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user_id") != null) {
			try {
				URL url = new URL("http://localhost:8080/TP6_LeBonAngle/api/user/" + session.getAttribute("user_id"));
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
				}
				BufferedReader br = new BufferedReader(
					new InputStreamReader((conn.getInputStream()))
				);
				
				String output;
				
				while ((output = br.readLine()) != null) {
					ObjectMapper mapper = new ObjectMapper();
					
					User currentUser = mapper.readValue(output, User.class);

					request.setAttribute("current_user", currentUser);
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			RequestDispatcher requestDispatcher;
			requestDispatcher = request.getRequestDispatcher("/WEB-INF/depose.jsp");
			requestDispatcher.forward(request, response);
		} else {
			RequestDispatcher requestDispatcher;
			requestDispatcher = request.getRequestDispatcher("/WEB-INF/depose_deconnecte.jsp");
			requestDispatcher.forward(request, response);
		}
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FabriqueAnnonce fabrique = new FabriqueAnnonce();
		try {
			Advertisement ad = fabrique.construitAnnonce(request);
			request.setAttribute("ad", ad);
			RequestDispatcher requestDispatcher;
			requestDispatcher = request.getRequestDispatcher("/WEB-INF/recap_annonce.jsp");
			requestDispatcher.forward(request, response);
		} catch(Exception e) {
			e.printStackTrace();
			request.setAttribute("erreurs", fabrique.getErreurs());
			RequestDispatcher requestDispatcher;
			requestDispatcher = request.getRequestDispatcher("/WEB-INF/depose.jsp");
			requestDispatcher.forward(request, response);
		}
	}
}
