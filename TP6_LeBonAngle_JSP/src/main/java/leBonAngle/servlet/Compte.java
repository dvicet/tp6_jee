package leBonAngle.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import leBonAngle.entities.User;

@WebServlet("/compte")
public class Compte extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
	public Compte() {
        super();
    }
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		if(session.getAttribute("user_id") != null) {
			// get current user
			try {
				URL url = new URL("http://localhost:8080/TP6_LeBonAngle/api/user/" + session.getAttribute("user_id"));
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
				}
				BufferedReader br = new BufferedReader(
					new InputStreamReader((conn.getInputStream()))
				);
				
				String output;
				
				while ((output = br.readLine()) != null) {
					ObjectMapper mapper = new ObjectMapper();
					
					User currentUser = mapper.readValue(output, User.class);

					request.setAttribute("current_user", currentUser);
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// get all users
		try {
			URL url = new URL("http://localhost:8080/TP6_LeBonAngle/api/user/tous");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(
				new InputStreamReader((conn.getInputStream()))
			);
			
			String output;
			
			while ((output = br.readLine()) != null) {
				ObjectMapper mapper = new ObjectMapper();
				
				List<User> users = mapper.readValue(output, new TypeReference<List<User>>() {});

				request.setAttribute("users", users);
				RequestDispatcher requestDispatcher;
				requestDispatcher = request.getRequestDispatcher("/WEB-INF/compte.jsp");
				requestDispatcher.forward(request, response);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("sign-up-pseudo") != null) {
			FabriqueUtilisateur fabrique = new FabriqueUtilisateur();
			try {
				User user = fabrique.construitUser(request);
				this.doGet(request, response);
			} catch(Exception e) {
				request.setAttribute("erreurs", fabrique.getErreurs());
				this.doGet(request, response);
			}
		} else if(request.getParameter("login-user") != null) {
			HttpSession session = request.getSession();
			session.setAttribute("user_id", request.getParameter("login-user"));

			this.doGet(request, response);
		}
	}
}
