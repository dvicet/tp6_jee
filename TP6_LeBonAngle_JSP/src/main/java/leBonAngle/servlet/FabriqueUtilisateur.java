package leBonAngle.servlet;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;

import leBonAngle.entities.User;

public class FabriqueUtilisateur {
	private static final String CHAMP_PSEUDO = "sign-up-pseudo";
	
	private Map<String, String> erreurs = new HashMap<String, String>();
	
	public boolean succesCreation;

	public User construitUser(HttpServletRequest request) throws Exception {
		String pseudo = request.getParameter(CHAMP_PSEUDO);
		
		// bidouille pour que ça soit bien encodé
		pseudo = URLEncoder.encode(pseudo, "ISO-8859-1");
		pseudo = URLDecoder.decode(pseudo, "UTF-8");
		
		try {
			validationPseudo(pseudo);
		} catch(Exception e) {
			erreurs.put(CHAMP_PSEUDO, e.getMessage());
		}
		
		succesCreation = erreurs.isEmpty();
		if(succesCreation) {
			User user = new User();
			user.setPseudo(pseudo);
			
			try {
				URL url = new URL("http://localhost:8080/TP6_LeBonAngle/api/user/insert");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.setDoOutput(true);
				
				ObjectMapper mapper = new ObjectMapper();
				
				try (var wr = new OutputStreamWriter(conn.getOutputStream() ,"UTF-8")) {
	                mapper.writeValue(wr, user);
	            }
				
				if (conn.getResponseCode() != 200) {
					erreurs.put("user-creation", "Erreur lors de la requête vers l'API");
					throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
				}
			} catch (MalformedURLException e) {
				erreurs.put("user-creation", "Erreur lors de la requête vers l'API");
				e.printStackTrace();
			} catch (IOException e) {
				erreurs.put("user-creation", "Erreur lors de la requête vers l'API");
				e.printStackTrace();
			}
			
			return user;
		} else {
			erreurs.put("user-creation", "Erreur lors de la création de l'utilisateur");
			throw new Exception("Erreur lors de la création de l'utilisateur");
		}
	}
	
	public void validationPseudo(String pseudo) throws Exception {
		if((pseudo.length() < 1) || (pseudo.length() > 255)) {
			throw new Exception("La longueur du pseudo doit être entre 1 et 255 caractères");
		}
	}
	
	public Map<String, String> getErreurs() {
		return erreurs;
	}
}
