package leBonAngle.servlet;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;

import leBonAngle.entities.Advertisement;

public class FabriqueAnnonce {
	private static final String CHAMP_TITRE = "title";
	private static final String CHAMP_URL_PHOTO = "url_photo";
	private static final String CHAMP_PRIX = "price";
	private static final String CHAMP_DESCRIPTION = "description";
	
	private Map<String, String> erreurs = new HashMap<String, String>();
	
	public boolean succesCreation;

	public Advertisement construitAnnonce(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		int id_user = Integer.parseInt((String) session.getAttribute("user_id"));
		String title = request.getParameter(CHAMP_TITRE);
		String url_photo = request.getParameter(CHAMP_URL_PHOTO);
		int price = Integer.parseInt(request.getParameter(CHAMP_PRIX));
		String description = request.getParameter(CHAMP_DESCRIPTION);
		
		
		// bidouille pour que ça soit bien encodé
		title = URLEncoder.encode(title, "ISO-8859-1");
		title = URLDecoder.decode(title, "UTF-8");
		url_photo = URLEncoder.encode(url_photo, "ISO-8859-1");
		url_photo = URLDecoder.decode(url_photo, "UTF-8");
		description = URLEncoder.encode(description, "ISO-8859-1");
		description = URLDecoder.decode(description, "UTF-8");
		
		try {
			validationPrix(price);
		} catch(Exception e) {
			erreurs.put(CHAMP_PRIX, e.getMessage());
		}
		
		succesCreation = erreurs.isEmpty();
		if(succesCreation) {
			Advertisement ad = new Advertisement();
			ad.setId_user(id_user);
			ad.setTitle(title);
			ad.setUrl_photo(url_photo);
			ad.setPrice(price);
			ad.setDescription(description);
			
			try {
				URL url = new URL("http://localhost:8080/TP6_LeBonAngle/api/annonce/create");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.setDoOutput(true);
				
				ObjectMapper mapper = new ObjectMapper();
				
				try (var wr = new OutputStreamWriter(conn.getOutputStream() ,"UTF-8")) {
	                mapper.writeValue(wr, ad);
	            }
				
				if (conn.getResponseCode() != 200) {
					erreurs.put("user-creation", "Erreur lors de la requête vers l'API");
					throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
				}
			} catch (MalformedURLException e) {
				erreurs.put("user-creation", "Erreur lors de la requête vers l'API");
				e.printStackTrace();
			} catch (IOException e) {
				erreurs.put("user-creation", "Erreur lors de la requête vers l'API");
				e.printStackTrace();
			}
			
			return ad;
		} else {
			erreurs.put("user-creation", "Erreur lors de la création de l'annonce");
			throw new Exception("Erreur lors de la création de l'annonce");
		}
	}
	
	public void validationPrix(int price) throws Exception {
		if((price < 0) || (1000000 < price)) {
			throw new Exception("Le prix doit être compris entre 0 et 1 000 000 €");
		}
	}
	
	public Map<String, String> getErreurs() {
		return erreurs;
	}
}
