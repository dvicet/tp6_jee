package leBonAngle.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONException;
import org.json.JSONObject;

import leBonAngle.entities.Advertisement;
import leBonAngle.entities.Comment;
import leBonAngle.entities.FabriqueComment;
import leBonAngle.entities.User;

@WebServlet("/annonces")
public class Annonces extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
	public Annonces() {
        super();
    }
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("id") == null) {
			// Toutes les annonces
			try {
				URL url = new URL("http://localhost:8080/TP6_LeBonAngle/api/annonce/tous");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
				}
				BufferedReader br = new BufferedReader(
					new InputStreamReader((conn.getInputStream()))
				);
				
				String output;
				
				while ((output = br.readLine()) != null) {
					ObjectMapper mapper = new ObjectMapper();
					
					List<Advertisement> ads = mapper.readValue(output, new TypeReference<List<Advertisement>>() {});

					request.setAttribute("ads", ads);
					RequestDispatcher requestDispatcher;
					requestDispatcher = request.getRequestDispatcher("/WEB-INF/annonces.jsp");
					requestDispatcher.forward(request, response);
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			// Annonce unique
			
			try {
				String id = request.getParameter("id");
				URL url = new URL("http://localhost:8080/TP6_LeBonAngle/api/annonce/" + id);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
				}
				BufferedReader br = new BufferedReader(
					new InputStreamReader((conn.getInputStream()))
				);
				
				String output;
				
				while ((output = br.readLine()) != null) {
					ObjectMapper mapper = new ObjectMapper();
					
					Advertisement ad = mapper.readValue(output, Advertisement.class);

					request.setAttribute("ad", ad);
				}
				
				try {
					//String id = request.getParameter("id");
					URL urluser = new URL("http://localhost:8080/TP6_LeBonAngle/api/user/tous");
					conn = (HttpURLConnection) urluser.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
					}
					BufferedReader br_user = new BufferedReader(
						new InputStreamReader((conn.getInputStream()))
					);
					
					String output_user;
					
					while ((output_user = br_user.readLine()) != null) {
						ObjectMapper mapper = new ObjectMapper();
						
						List<User> users = mapper.readValue(output_user,new TypeReference<List<User>>() {});
						request.setAttribute("users", users);
						for(User u : users)
						{
							System.out.println(u.getPseudo());
						}
					}
					
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
					
				//Get comments
				try {
					//String id = request.getParameter("id");
					URL url_comment = new URL("http://localhost:8080/TP6_LeBonAngle/api/comment/" + id);
					conn = (HttpURLConnection) url_comment.openConnection();
					conn.setRequestMethod("GET");
					conn.setRequestProperty("Accept", "application/json");
					if (conn.getResponseCode() != 200) {
						throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
					}
					BufferedReader br_comments = new BufferedReader(
						new InputStreamReader((conn.getInputStream()))
					);
					
					String output_comments;
					
					while ((output_comments = br_comments.readLine()) != null) {
						ObjectMapper mapper = new ObjectMapper();
						
						List<Comment> comments = mapper.readValue(output_comments, new TypeReference<List<Comment>>() {});

						request.setAttribute("comments", comments);
					}
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				RequestDispatcher requestDispatcher;
				requestDispatcher = request.getRequestDispatcher("/WEB-INF/annonce.jsp");
				requestDispatcher.forward(request, response);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
				
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		FabriqueComment fabrique = new FabriqueComment();
		//HttpSession session = request.getSession(false);
		RequestDispatcher requestDispatcher;
		//System.out.println("userid:" + Integer.parseInt(session.getAttribute("user_id").toString()));
		//Comment comment = new Comment();
		Comment comment = fabrique.createComment(request);
		
		if(fabrique.succesCreation)
		{
			try {
				URL url = new URL("http://localhost:8080/TP6_LeBonAngle/api/comment/insert");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Accept", "application/json");
				conn.setDoOutput(true);
				conn.setRequestProperty("Content-type", "application/json");
				
				JSONObject payload = new JSONObject();
				payload.put("id_advertisement", comment.getId_advertisement());
				payload.put("id_user", comment.getId_user());
				payload.put("comment", comment.getComment());
				payload.put("price_proposed", comment.getPrice_proposed());

				try(OutputStreamWriter os = new OutputStreamWriter(conn.getOutputStream()))
				{
					os.write(payload.toString());
					os.flush();
				}
				
				
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
				}		
				
				request.setAttribute("id", request.getParameter("id"));
				
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			request.setAttribute("fabrique", fabrique);
		}
		this.doGet(request, response);
	
	}
}
