<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>LeBonAngle - Annonces</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<%@ include file = "header.jsp" %>
	<h1>Annonces</h1>
	<p>Voici la liste des annonces présentes sur LeBonAngle :<p>
	
	<%@ page import="leBonAngle.entities.Advertisement" %>
	<%@ page import="java.util.List" %> 
	<%
	List<Advertisement> ads = (List<Advertisement>) request.getAttribute("ads");
	for(Advertisement a:ads) {
	%>
	<div class="annonce">
		<div>
			<a href="annonces?id=<%= a.getId() %>">
				<h2><%= a.getTitle() %></h2>
			</a>
			<p>Prix : <span class="prix"><%= a.getPrice() %> €</span></p>
		</div>
		<a href="annonces?id=<%= a.getId() %>">
			<img class="image-mini" src="<%= a.getUrl_photo() %>" />
		</a>
	</div>
	<% } %>
</body>
</html>