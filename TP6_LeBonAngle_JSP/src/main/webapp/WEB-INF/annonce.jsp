<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   	<%@ page import="java.util.List" %> 
  	<%@ page import="leBonAngle.entities.Comment" %>
  	<%@ page import="leBonAngle.entities.User" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>LeBonAngle - ${requestScope.ad.getTitle()}</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<%@ include file = "header.jsp" %>
	<h1>${requestScope.ad.getTitle()}</h1>
	<img class="image-grande" src="${requestScope.ad.getUrl_photo()}"/>
	<p><span class="prix">${requestScope.ad.getPrice()} €</span></p>
	<p>${requestScope.ad.getDescription()}</p>

	<span style="color:red">${fabrique.getMap("SESSION")}</span>
	<span class="comment_title"><h2>Poster un commentaire : </h2></span>
	<form class="form_comment" method="POST" action=<%request.getRequestURI(); %> >

	<input class="prix_proposé" type="text" id="prix" name="prix" placeholder="Prix à proposé (€)"/>
	<textarea class="textarea" id="comment" name="comment"></textarea>
	<input type="submit" value="Poster mon commentaire">
	</form>

	<span class="comment_title"><h2>Commentaires : </h2></span>
	<%
	List<Comment> comments = (List<Comment>) request.getAttribute("comments");
	List<User> users = (List<User>) request.getAttribute("users");
	for(Comment c:comments)
	{
	%>
	<div class="comment">
		<div>
		<span class="comment_user"><%=users.get(c.getId_user()-1).getPseudo() %></span>
		<span class="prix">Prix proposé: <%=c.getPrice_proposed() %>€</span>
		</div>
		<div>
		<span class="color:black;"><%=c.getComment()%></span>
		</div>
	</div>
	<%
	}
	%>

	
	
	
</body>
</html>