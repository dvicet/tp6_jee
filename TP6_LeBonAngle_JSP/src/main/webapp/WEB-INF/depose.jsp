<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>LeBonAngle - Déposer une annonce</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<%@ include file = "header.jsp" %>
	<h1>Déposer une annonce</h1>
	<p>Vous publiez en tant que : ${requestScope.current_user.getPseudo()}</p>
	<form action="" method="post">
		<label for="title">Titre :</label>
	    <input type="text" name="title" id="title">
	    <br />
	    <label for="url_photo">URL de la photo :</label>
	    <input type="text" name="url_photo" id="url_photo">
	    <br />
	    <label for="price">Prix en euros :</label>
		<input type="number" id="price" name="price" min="0">
		<p class="error">${requestScope.erreurs.get("price")}</p>
	    <br />
	    <label for="description">Description :</label>
	    <textarea name="description" id="description"></textarea>
	    <br />
		<input type="submit" value="Déposer">
	</form>
</body>
</html>