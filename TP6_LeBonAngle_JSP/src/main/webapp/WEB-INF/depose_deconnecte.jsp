<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>LeBonAngle - Déposer une annonce</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<%@ include file = "header.jsp" %>
	<p>Vous devez être connecté pour poster une annonce</p>
	<p><a href="compte">Gérer mon compte</a></p>
</body>
</html>