<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>LeBonAngle - Gérer mon compte</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<%@ include file = "header.jsp" %>
	<h1>Gérer mon compte</h1>
	<%@ page import="java.util.Map" %>
	<% if(request.getAttribute("current_user") != null) { %>
	<p>Vous êtes actuellement connecté avec : ${requestScope.current_user.getPseudo()}</p>
	<% } %>
	<h2>Se connecter avec un compte existant</h2>
	<form action="" method="post">
		<label for="login-user">Choisissez un utilisateur :</label>

		<select name="login-user" id="login-user">
			<%@ page import="leBonAngle.entities.User" %>
			<%@ page import="java.util.List" %>
			<%
			List<User> users = (List<User>) request.getAttribute("users");
			for(User u:users) {
			%>
		    <option value="<%= u.getId() %>"><%= u.getPseudo() %></option>
		    <% } %>
		</select>
		<br />
		<input type="submit" value="Se connecter avec cet utilisateur">
	</form>
	
	<br />
	<h2>Créer un compte</h2>
	<form action="" method="post">
		<label for="sign-up-pseudo">Pseudo :</label>

		<input type="text" name="sign-up-pseudo" id="sign-up-pseudo">
		<% if(request.getAttribute("erreurs") != null) {
			Map<String, String> erreurs = (Map<String, String>) request.getAttribute("erreurs");
			if(erreurs.get("sign-up-pseudo") != null) { %>
		<p class="error">${requestScope.erreurs.get("sign-up-pseudo")}</p>
		<% }} %>
		<br />
		<input type="submit" value="Créer un nouvel utilisateur">
		<% if(request.getAttribute("erreurs") != null) {
			Map<String, String> erreurs = (Map<String, String>) request.getAttribute("erreurs");
			if(erreurs.get("user-creation") != null) { %>
		<p class="error">${requestScope.erreurs.get("user-creation")}</p>
		<% }} %>
		
	</form>
</body>
</html>