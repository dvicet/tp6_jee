package dao;

import java.util.List;

import entities.Comment;

public interface ICommentDAO {
	public void insert(Comment c);
	public void delete(Comment c);
	public Comment getComment(int id);
	public List<Comment> getComments(int idAd);
}
