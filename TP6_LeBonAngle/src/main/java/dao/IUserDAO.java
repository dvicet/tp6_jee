package dao;

import java.util.List;

import entities.User;

public interface IUserDAO {
	public void insert(User u);
	public void delete(User u);
	public List<User> getAllUsers();
	public User getOneUser(int id);
}
