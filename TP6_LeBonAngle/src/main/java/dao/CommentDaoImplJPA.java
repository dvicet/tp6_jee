package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.transaction.Transactional;

import entities.Comment;

public class CommentDaoImplJPA implements ICommentDAO {
	private EntityManager entityManager;
	private EntityTransaction tx;
	
	public CommentDaoImplJPA() {
		EntityManagerFactory emf;
		emf = Persistence.createEntityManagerFactory("UnitLeBonAngle");
		this.entityManager = emf.createEntityManager();
		this.tx = entityManager.getTransaction();
	}	

	@Override 
	public void insert(Comment c) {
		tx.begin();
		entityManager.merge(c);
		tx.commit();
	}

	@Override
	public void delete(Comment c) {
		tx.begin();
		entityManager.remove(entityManager.contains(c) ? c : entityManager.merge(c));
	//	entityManager.remove(c);
		tx.commit();
	}

	@Override
	public List<Comment> getComments(int idAd) {
		Query q = entityManager.createQuery("SELECT c FROM Comment c WHERE c.id_advertisement = :id");
		q.setParameter("id", idAd);
		List<Comment> comments = q.getResultList();
		return comments;
	}

	@Override
	public Comment getComment(int id) {
		Query q = entityManager.createQuery("SELECT c FROM Comment c WHERE c.id = :id");
		q.setParameter("id", id);
		Comment comment = (Comment) q.getSingleResult();
		return comment;
	}
	
}
