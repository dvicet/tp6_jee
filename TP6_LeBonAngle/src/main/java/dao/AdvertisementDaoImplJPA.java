package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Advertisement;
import entities.Comment;

public class AdvertisementDaoImplJPA implements IAdvertisementDAO {
	private EntityManager entityManager;
	private EntityTransaction tx;
	public AdvertisementDaoImplJPA() {
		EntityManagerFactory emf;
		emf = Persistence.createEntityManagerFactory("UnitLeBonAngle");
		this.entityManager = emf.createEntityManager();
		this.tx = entityManager.getTransaction();
	}	
	
	@Override
	public void insert(Advertisement ad) {
		tx.begin();
		entityManager.merge(ad);
		tx.commit();	
	}

	@Override
	public List<Advertisement> getAds() {
		Query q = entityManager.createQuery("SELECT a FROM Advertisement a");
		List<Advertisement> ads = q.getResultList();
		return ads;
	}

	@Override
	public Advertisement getAd(int id) {
		Query q = entityManager.createQuery("SELECT a FROM Advertisement a WHERE a.id = :id");
		q.setParameter("id", id);
		Advertisement ad = (Advertisement) q.getSingleResult();
		return ad;
	}

	@Override
	public void deleteAd(Advertisement ad) {
		tx.begin();
		Query q = entityManager.createQuery("SELECT c FROM Comment c WHERE c.id_advertisement = :id");
		q.setParameter("id", ad.getId());
		List<Comment> comments = q.getResultList();
		for(Comment c:comments) {
			entityManager.remove(c);
		}
		
	//	entityManager.remove(ad);
		entityManager.remove(entityManager.contains(ad) ? ad : entityManager.merge(ad));
		tx.commit();
	}

	@Override
	public void update(Advertisement ad) {
		tx.begin();
		entityManager.merge(ad);
		tx.commit();
	}
	
}
