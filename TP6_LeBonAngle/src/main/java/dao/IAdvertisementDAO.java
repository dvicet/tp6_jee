package dao;

import java.util.List;

import entities.Advertisement;

public interface IAdvertisementDAO {
	public void insert(Advertisement ad);
	public List<Advertisement> getAds();
	public Advertisement getAd(int id);
	public void deleteAd(Advertisement ad);
	public void update(Advertisement ad);
}
