package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import entities.Advertisement;
import entities.Comment;
import entities.User;

public class UserDaoImplJPA implements IUserDAO {
	private EntityManager entityManager;
	private EntityTransaction tx;
	
	public UserDaoImplJPA() {
		EntityManagerFactory emf;
		emf = Persistence.createEntityManagerFactory("UnitLeBonAngle");
		this.entityManager = emf.createEntityManager();
		this.tx = entityManager.getTransaction();
	}

	@Override
	public void insert(User u) {
		tx.begin();
		entityManager.merge(u);
		tx.commit();
	}
	
	@Override
	public void delete(User u) {
		tx.begin();

		AdvertisementDaoImplJPA adDao = new AdvertisementDaoImplJPA();
		CommentDaoImplJPA commentDao = new CommentDaoImplJPA();
		
		Query q = entityManager.createQuery("SELECT c FROM Comment c WHERE c.id_user = :id");
		q.setParameter("id", u.getId());
		List<Comment> comments = q.getResultList();
		for(Comment c:comments) {
			commentDao.delete(c);
		}
		
		q = entityManager.createQuery("SELECT ad FROM Advertisement ad WHERE ad.id_user = :id");
		q.setParameter("id", u.getId());
		List<Advertisement> ads = q.getResultList();
		for(Advertisement ad : ads) {
			adDao.deleteAd(ad);
		}
		
		entityManager.remove(entityManager.contains(u) ? u : entityManager.merge(u));
		tx.commit();
	}

	@Override
	public List<User> getAllUsers() {
		Query q = entityManager.createQuery("SELECT u FROM User u");
		List<User> users = q.getResultList();
		return users;
	}
	

	@Override
	public User getOneUser(int id) {
		Query q = entityManager.createQuery("SELECT u FROM User u WHERE u.id = :id");
		q.setParameter("id", id);
		User u = (User) q.getSingleResult();
		return u;
	}


}
