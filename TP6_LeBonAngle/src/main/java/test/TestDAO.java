package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import dao.AdvertisementDaoImplJPA;
import dao.CommentDaoImplJPA;
import dao.IAdvertisementDAO;
import dao.ICommentDAO;
import dao.IUserDAO;
import dao.UserDaoImplJPA;
import entities.Advertisement;
import entities.Comment;
import entities.User;

public class TestDAO {
	
	@Test
	public void testInsertUser() {
		User user = new User();
		user.setPseudo("jean");
		
		IUserDAO dao = new UserDaoImplJPA();
		dao.insert(user);
		
		boolean userFound = false;
		
		List<User> users = dao.getAllUsers();
		for(User u:users) {
			if(user.getPseudo() == "jean") {
				userFound = true;
				break;
			}
		}
		assertTrue(userFound);
	}
	
	@Test
	public void testGetAllUsers() {
		IUserDAO dao = new UserDaoImplJPA();
		List<User> users = dao.getAllUsers();
		for(User u:users) {
			System.out.println(u.getPseudo());
		}
	}
	
	@Test
	public void testGetOneUser() {
		IUserDAO dao = new UserDaoImplJPA();
		User user = dao.getOneUser(1);
		assertEquals("Adrien", user.getPseudo());
	}
	
	@Test
	public void testDeleteUser() {
		User user = new User();
		user.setPseudo("Michel");
		
		IUserDAO dao = new UserDaoImplJPA();
		dao.insert(user);
		
		User userToDelete = dao.getOneUser(4);
		dao.delete(userToDelete);
	}
	
	@Test
	public void testAdvertisement() {
		IAdvertisementDAO dao = new AdvertisementDaoImplJPA();
		
		//get
		Advertisement ad = dao.getAd(2);
		assertEquals(ad.getTitle(), "Trotinnette Electrique");
		
		//getAll
		List<Advertisement> ads = dao.getAds();
		for(Advertisement a:ads) {
			System.out.println(a.getTitle());
		}
		
		//update
		ad.setTitle("Trotinette sans chargeur");
		dao.update(ad);
		ad = dao.getAd(2);
		assertEquals(ad.getTitle(), "Trotinette sans chargeur");
		
		//insert
		Advertisement insertedAd = new Advertisement();
		insertedAd.setId_user(1);
		insertedAd.setTitle("Donne chaton adorable");
		insertedAd.setDescription("mignon");
		insertedAd.setUrl_photo("https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/1-month-old_kittens_32.jpg/1280px-1-month-old_kittens_32.jpg");
		insertedAd.setPrice(0);
		dao.insert(insertedAd);
		
		//delete
		dao.deleteAd(ad);
	}
	
	@Test
	public void testComment() {
		ICommentDAO dao = new CommentDaoImplJPA();
		
		//get all comments from an ad
		List<Comment> comments = dao.getComments(1);
		for(Comment c:comments) {
			System.out.println(c.getComment());
		}
		
		//get one comment and delete it
		Comment comment = dao.getComment(3);
		assertEquals(comment.getComment(), "Un vélo n'a pas besoin de chargeur, je suis interessé");
		dao.delete(comment);
		
		//insert
		Comment insertedComment = new Comment();
		insertedComment.setId_user(1);
		insertedComment.setId_advertisement(3);
		insertedComment.setComment("Attendez en fait je le garde");
		insertedComment.setPrice_proposed(0);
		dao.insert(insertedComment);
	}
}
