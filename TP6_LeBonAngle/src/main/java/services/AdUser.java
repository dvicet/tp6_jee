package services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dao.UserDaoImplJPA;
import entities.User;

@Path("/user")
public class AdUser {

	private static UserDaoImplJPA dao = new UserDaoImplJPA();
	
	@Path("/tous")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getAllUsers()
	{
		return this.dao.getAllUsers();
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public User getOneUser(@PathParam("id") Integer Id)
	{
		return this.dao.getOneUser(Id);
	}
	
	@Path("/insert")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public User insert(User user) throws Exception
	{
		// Vérifie si l'utilisateur n'est pas déjà présent
		List<User> users = dao.getAllUsers();
		for(User u:users) {
			if(u.getPseudo().equalsIgnoreCase(user.getPseudo())) {
				throw new Exception("Utilisateur déjà existant");
			}
		}
		this.dao.insert(user);
		
		return user;
	}
	
	@Path("/del")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void delete(User u)
	{
		this.dao.delete(u);
	}
	
	
}
