package services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dao.AdvertisementDaoImplJPA;
import dao.UserDaoImplJPA;
import entities.Advertisement;


@Path("/annonce")
public class AdService {

	//private static UserDaoImplJPA dao = new UserDaoImplJPA();
	private static AdvertisementDaoImplJPA adDao = new AdvertisementDaoImplJPA();
	
	//Renvoie toutes les annonces
	@Path("/tous")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Advertisement> ReadAds()
	{
		return this.adDao.getAds();
	}
	
	
	//Renvoie l'annonce associée à l'id
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Advertisement ReadAd(@PathParam("id") Integer Id)
	{
		return this.adDao.getAd(Id);
	}
	
	@Path("/create")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Advertisement create(Advertisement a)
	{
		this.adDao.insert(a);
		return a;
	}
	
	//Supprimer une annonce
	@Path("/delete")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void delete(Advertisement a)
	{
		this.adDao.deleteAd(a);
	}
	
	
	//Modifier une annonce
	@Path("/update")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(Advertisement a)
	{
		this.adDao.update(a);
	}
}
