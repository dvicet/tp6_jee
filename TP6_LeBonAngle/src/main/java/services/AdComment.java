package services;

import java.util.List;

import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import dao.CommentDaoImplJPA;
import dao.UserDaoImplJPA;
import entities.Advertisement;
import entities.Comment;


@Path("/comment")
public class AdComment {

	private static CommentDaoImplJPA addao = new CommentDaoImplJPA();
	
	// Renvoie les commentaires de l'annonce
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> ReadCmts(@PathParam("id") Integer Id)
	{
		return this.addao.getComments(Id);
	}
	
	//Renvoie le commentaire associee a� l'id demand�
		@Path("/id/{id}")
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public Comment ReadCmt(@PathParam("id") Integer Id)
		{
			return this.addao.getComment(Id);
		}
	
	// Supprime le commentaire par son identifiant
	@Path("/del")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void delete(Comment id)
    {
        this.addao.delete(id);
    }
	
	@Path("/insert")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Comment create(Comment c)
	{
		this.addao.insert(c);
		return c;
	}

}
