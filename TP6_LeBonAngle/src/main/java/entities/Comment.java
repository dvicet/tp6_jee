package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity @Table(name ="Comments")
public class Comment {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int id_advertisement;
	private int id_user;
	private String comment;
	private int price_proposed;
	
	public Comment() {};
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_advertisement() {
		return id_advertisement;
	}
	public void setId_advertisement(int id_advertisement) {
		this.id_advertisement = id_advertisement;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getPrice_proposed() {
		return price_proposed;
	}
	public void setPrice_proposed(int price_proposed) {
		this.price_proposed = price_proposed;
	}
}
