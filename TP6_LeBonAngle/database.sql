DROP DATABASE IF EXISTS LeBonAngle;
CREATE DATABASE LeBonAngle;
USE LeBonAngle;

CREATE TABLE Users
(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	pseudo VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE Advertisements
(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	id_user INT NOT NULL,
	title VARCHAR(500) NOT NULL,
	description TEXT(65000),
	url_photo VARCHAR(255),
	price INT NOT NULL,
	FOREIGN KEY (id_user) REFERENCES Users(id)
);

CREATE TABLE Comments
(
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	id_user INT NOT NULL,
	id_advertisement INT NOT NULL,
	comment TEXT(65000),
	price_proposed INT NOT NULL,
	FOREIGN KEY (id_user) REFERENCES Users(id),
	FOREIGN KEY (id_advertisement) REFERENCES Advertisements(id)
);

INSERT INTO Users(pseudo) VALUES
('Adrien'),('Anatole'),('Damien');

INSERT INTO Advertisements(id_user, title, description,url_photo, price) VALUES
(1, 'Clio 2 aka la fusée Arianne','Voiture dantesque aux multiples atouts',"https://www.france-troc.com/ImgUsers/annonces/2009/11/369825/kbxvmd.JPG" , 4000),
(2, 'Trotinnette Electrique','J\'ai perdu le chargeur ...',"https://cdn.toptrottinette.com/wp-content/uploads/2016/06/e-twow_master_noir_trottinette_electrique-195x300.jpg", 650),
(3, 'Vélo de course','Je cours plus vite que si j\'étais sur le vélo donc je le vends',"http://www.allwhitebackground.com/images/1/1656.jpeg", 175);

INSERT INTO Comments(id_user, id_advertisement, comment, price_proposed) VALUES
(1,2,'Je peux te vendre le chargeur si tu veux prix à débattre', 50),
(3,2,'Je suis intéressé, moi j\'ai perdu la trotinette', 600),
(2,3,'Un vélo n\'a pas besoin de chargeur, je suis interessé', 160),
(1,3,'Il est trop beau ce vélo je le veux !', 200);
